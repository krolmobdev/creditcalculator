package com.example.creditcalc.controllers;

import com.example.creditcalc.services.CreditParametersRangeService;
import com.example.creditcalc.model.CreditParameters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.List;

@Controller
@RequestMapping("/")
public class HomeController {
    private CreditParametersRangeService rangeService;

    public HomeController(@Autowired CreditParametersRangeService rangeService) {
        this.rangeService = rangeService;
    }

    @ModelAttribute(name = "annualRates")
    public List<BigDecimal> annualRateValues() {
        return rangeService.getAnnualRateValues();
    }


    @ModelAttribute(name = "terms")
    public List<Integer> termValues() {
        return rangeService.getTermValues();
    }

    @ModelAttribute(name = "creditParameters")
    public CreditParameters creditParameters() {
        return new CreditParameters();
    }

    @GetMapping
    public String home() {
        return "home";
    }

    @PostMapping
    public String processCreditParameters(@ModelAttribute("creditParameters") @Valid CreditParameters creditParameters,
                                          Errors errors, RedirectAttributes attributes) {
        if (errors.hasErrors()) {
            return "home";
        }

        attributes.mergeAttributes(creditParameters.toMap());

        return "redirect:/repaymentSchedule";
    }
}
