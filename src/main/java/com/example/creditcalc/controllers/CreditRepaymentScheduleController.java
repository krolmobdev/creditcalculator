package com.example.creditcalc.controllers;

import com.example.creditcalc.calculators.CreditRepaymentScheduleCalculator;
import com.example.creditcalc.model.CreditParameters;
import com.example.creditcalc.model.CreditPayment;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.util.List;

@Slf4j
@Controller
@RequestMapping("/repaymentSchedule")
public class CreditRepaymentScheduleController {

    @GetMapping
    public String displayRepaymentSchedule(@Valid CreditParameters creditParameters, Model model) {
        model.addAttribute("creditParameters", creditParameters);
        model.addAttribute("creditPayments", getCreditPayments(creditParameters));

        return "repaymentSchedule";
    }

    private List<CreditPayment> getCreditPayments(CreditParameters creditParameters) {
        CreditRepaymentScheduleCalculator calculator = new CreditRepaymentScheduleCalculator(creditParameters);

        return calculator.calculateCreditPayments();
    }
}
