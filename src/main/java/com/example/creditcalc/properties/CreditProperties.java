package com.example.creditcalc.properties;

public class CreditProperties {
    private CreditProperties () {};

    public static class Sum {
        public static final String MIN = "100000";
        public static final String MAX = "5000000";
    }

    public static class Term {
        public static final int MIN = 12;
        public static final int MAX = 60;
    }

    public static class AnnualRate {
        public static final String MIN = "12.9";
        public static final String MAX = "23.9";
    }
}
