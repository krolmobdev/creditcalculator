package com.example.creditcalc.calculators;

import com.example.creditcalc.model.CreditParameters;

import java.math.BigDecimal;
import java.math.MathContext;

import static java.math.RoundingMode.HALF_EVEN;

class CreditAnnuityPaymentCalculator {
    private CreditParameters creditParameters;

    CreditAnnuityPaymentCalculator(CreditParameters creditParameters) {
        this.creditParameters = creditParameters;
    }

    BigDecimal calculateAnnuityPayment() {
        BigDecimal sum = creditParameters.getSum();
        BigDecimal annuityCoefficient = getAnnuityCoefficient();

        return sum.multiply(annuityCoefficient).setScale(2, HALF_EVEN);
    }

    private BigDecimal getAnnuityCoefficient() {
        BigDecimal monthlyRate = AnnualRateOperations.getStakeMonthlyRate(creditParameters.getAnnualRate());
        BigDecimal coefficientDenominator = getAnnuityCoefficientDenominator(monthlyRate);

        return monthlyRate.divide(coefficientDenominator, MathContext.DECIMAL128);
    }

    private BigDecimal getAnnuityCoefficientDenominator(BigDecimal monthlyRate) {
        int term = creditParameters.getTerm();

        return BigDecimal.ONE.subtract(BigDecimal.ONE.add(monthlyRate).pow(-term, MathContext.DECIMAL128));
    }
}
