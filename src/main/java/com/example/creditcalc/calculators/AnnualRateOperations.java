package com.example.creditcalc.calculators;

import java.math.BigDecimal;

class AnnualRateOperations {
    private static final int MONTH_IN_YEAR = 12;

    static BigDecimal getStakeMonthlyRate(BigDecimal percentAnnualRate) {
        BigDecimal stakeAnnualRate = getStakeAnnualRate(percentAnnualRate);
        return stakeAnnualRate.divide(new BigDecimal(MONTH_IN_YEAR), BigDecimal.ROUND_HALF_EVEN);
    }

    private static BigDecimal getStakeAnnualRate(BigDecimal percentAnnualRate) {
        return percentAnnualRate.divide(BigDecimal.valueOf(100),BigDecimal.ROUND_HALF_EVEN);
    }
}
