package com.example.creditcalc.calculators;

import com.example.creditcalc.model.CreditParameters;
import com.example.creditcalc.model.CreditPayment;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class CreditRepaymentScheduleCalculator {
    private CreditParameters creditParameters;

    public CreditRepaymentScheduleCalculator(CreditParameters creditParameters) {
        this.creditParameters = creditParameters;
    }

    public List<CreditPayment> calculateCreditPayments() {
        List<CreditPayment> creditPayments = new ArrayList<>();
        creditPayments.add(calculateFirstPayment());

        for (int i = 1; i < creditParameters.getTerm(); i++) {
            CreditPayment previousPayment = creditPayments.get(i - 1);
            creditPayments.add(calculateCurrentPayment(previousPayment));
        }

        return creditPayments;
    }

    private CreditPayment calculateCurrentPayment(CreditPayment previousPayment) {
        BigDecimal totalPayment = previousPayment.getTotalPayment();
        BigDecimal principalBalance = previousPayment.getPrincipalBalance().subtract(totalPayment);
        BigDecimal interestPayment = calculateInterestPayment(principalBalance);
        BigDecimal principalPayment = calculatePrincipalPayment(totalPayment, interestPayment);
        LocalDate date = previousPayment.getDate().plusMonths(1);
        int number = previousPayment.getNumber();
        number ++;

        return new CreditPayment(number, date, principalPayment, interestPayment, principalBalance, totalPayment);
    }

    private CreditPayment calculateFirstPayment() {
        CreditAnnuityPaymentCalculator paymentCalculator = new CreditAnnuityPaymentCalculator(creditParameters);
        BigDecimal totalPayment = paymentCalculator.calculateAnnuityPayment();

        BigDecimal principalBalance = creditParameters.getSum().subtract(totalPayment);
        BigDecimal interestPayment = calculateInterestPayment(principalBalance);
        BigDecimal principalPayment = calculatePrincipalPayment(totalPayment, interestPayment);
        LocalDate date = LocalDate.now().plusMonths(1);
        int number = 1;

        return new CreditPayment(number, date, principalPayment, interestPayment, principalBalance, totalPayment);
    }

    private BigDecimal calculatePrincipalPayment(BigDecimal totalPayment, BigDecimal interestPayment) {
        return totalPayment.subtract(interestPayment);
    }

    private BigDecimal calculateInterestPayment(BigDecimal principalBalance) {
        return principalBalance.multiply(AnnualRateOperations.getStakeMonthlyRate(creditParameters.getAnnualRate()))
                .setScale(2, RoundingMode.HALF_EVEN);
    }
}
