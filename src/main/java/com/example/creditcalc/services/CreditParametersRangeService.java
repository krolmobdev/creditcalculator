package com.example.creditcalc.services;

import com.example.creditcalc.properties.CreditProperties;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
public class CreditParametersRangeService {
    public List<Integer> getTermValues() {
        return IntStream.rangeClosed(CreditProperties.Term.MIN, CreditProperties.Term.MAX)
                .boxed()
                .collect(Collectors.toList());
    }

    public List<BigDecimal> getAnnualRateValues() {
        List<Double> rateValues = new ArrayList<>();
        double startValue = Double.parseDouble(CreditProperties.AnnualRate.MIN);
        double endValue = Double.parseDouble(CreditProperties.AnnualRate.MAX);

        for (double v = startValue; v <= endValue; v = v + 0.1) {
            rateValues.add(v);
        }

        return rateValues.stream()
                .map(v -> new BigDecimal(v).setScale(2, RoundingMode.HALF_EVEN))
                .collect(Collectors.toList());
    }
}
