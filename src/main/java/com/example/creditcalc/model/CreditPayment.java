package com.example.creditcalc.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@AllArgsConstructor
public class CreditPayment {
    private int number;
    private LocalDate date;
    private BigDecimal principalPayment;
    private BigDecimal interestPayment;
    private BigDecimal principalBalance;
    private BigDecimal totalPayment;
}
