package com.example.creditcalc.model;

import com.example.creditcalc.properties.CreditProperties;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Map;

@Data
public class CreditParameters {
    @DecimalMin(value=CreditProperties.Sum.MIN)
    @DecimalMax(CreditProperties.Sum.MAX)
    @NotNull(message = "Укажите сумму кредита")
    private BigDecimal sum;

    @Range(min = CreditProperties.Term.MIN, max = CreditProperties.Term.MAX)
    private int term;

    @DecimalMin(CreditProperties.AnnualRate.MIN)
    @DecimalMax(CreditProperties.AnnualRate.MAX)
    private BigDecimal annualRate;

    public Map<String, Object> toMap() {
        return new ObjectMapper().convertValue(this, Map.class);
    }
}
